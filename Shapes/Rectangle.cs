﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    class Rectangle
    {
        public double height;
        public double width;
        public Rectangle(double h, double w)
        {
            height = h;
            width = w;
        }

        public double findPermit(double h, double w)
        {
            double perm = 2 * (h + w);
            return perm;

        }
        public double Area (double h, double w)
        {
            double areas = (h * w);
            return areas;
        }
            
            
        
    }
}
